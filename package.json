{
  "private": false,
  "name": "@megabytelabs/ansible-molecule-json",
  "description": "An npm package that converts the terminal output from an Ansible Molecule test into a JSON compatibility matrix",
  "license": "MIT",
  "author": "Brian Zalewski <help@megabyte.space> (https://megabyte.space)",
  "homepage": "https://megabyte.space",
  "repository": {
    "type": "git",
    "url": "git+https://gitlab.com/megabyte-labs/npm/ansible-molecule-json.git"
  },
  "bugs": {
    "url": "https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/issues",
    "email": "help@megabyte.space"
  },
  "version": "0.0.3",
  "main": "build/main/index.js",
  "module": "build/module/index.js",
  "bin": {
    "ansible-molecule-json": "bin/cli"
  },
  "files": [
    "!**/*.json",
    "!**/*.spec*",
    "build/main",
    "build/module"
  ],
  "scripts": {
    "build": "run-s clean && run-p build:*",
    "build:main": "tsc -p tsconfig.json",
    "build:module": "tsc -p tsconfig.module.json",
    "clean": "trash build test",
    "commit": "git-cz",
    "cov": "run-s build test:unit cov:html cov:lcov && open-cli coverage/index.html",
    "cov:check": "nyc report && nyc check-coverage --lines 100 --functions 100 --branches 100",
    "cov:html": "nyc report --reporter=html",
    "cov:lcov": "nyc report --reporter=lcov",
    "cov:send": "run-s cov:lcov && codecov",
    "describe": "npm-scripts-info",
    "doc": "run-s doc:html && open-cli build/docs/index.html",
    "doc:html": "typedoc src/ --exclude \"**/*.spec.ts\" --out build/docs",
    "doc:json": "typedoc src/ --exclude \"**/*.spec.ts\" --json build/docs/typedoc.json",
    "fix": "run-s fix:*",
    "fix:eslint": "eslint src --ext .ts --fix",
    "fix:prettier": "prettier \"src/**/*.ts\" --write",
    "info": "npm-scripts-info",
    "postinstall": "husky install && run-s update",
    "prepare-release": "run-s reset test doc:html version",
    "postpublish": "pinst --enable",
    "prepublishOnly": "pinst --disable",
    "reset": "git clean -dfx && git reset --hard && npm i --ignore-scripts",
    "test": "run-s build test:*",
    "test:eslint": "eslint src --ext .ts",
    "test:prettier": "prettier \"src/**/*.ts\" --list-different",
    "test:unit": "nyc --silent ava",
    "update": "bash .start.sh && cp ./.modules/npm/.start.sh .start.sh",
    "version": "standard-version --no-verify",
    "watch": "run-s clean && run-p watch:build",
    "watch:build": "tsc -p tsconfig.json -w",
    "watch:test": "nyc --silent ava --watch"
  },
  "config": {
    "commitizen": {
      "path": "cz-conventional-changelog"
    }
  },
  "typings": "build/main/index.d.ts",
  "dependencies": {
    "class-validator": "^0.13.1",
    "commander": "^7.1.0",
    "date-fns": "^2.21.3",
    "dotenv": "^8.2.0",
    "inquirer": "^8.0.0",
    "json-stringify-pretty-compact": "^3.0.0",
    "jsonfile": "^6.1.0",
    "optionator": "^0.9.1",
    "pino": "^6.11.1",
    "pino-pretty": "^4.5.0",
    "update-notifier": "^5.1.0"
  },
  "devDependencies": {
    "@ava/typescript": "^1.1.1",
    "@istanbuljs/nyc-config-typescript": "^1.0.1",
    "@megabytelabs/prettier-config": "^1.2.893",
    "@types/inquirer": "^7.3.1",
    "@types/node": "^15.0.1",
    "@types/update-notifier": "^5.0.0",
    "@types/validate-npm-package-name": "^3.0.0",
    "@typescript-eslint/eslint-plugin": "^4.15.2",
    "@typescript-eslint/eslint-plugin-tslint": "^4.22.0",
    "@typescript-eslint/parser": "^4.15.2",
    "ava": "^3.12.1",
    "codecov": "^3.8.1",
    "consola": "^2.15.3",
    "cspell": "^5.3.3",
    "cz-conventional-changelog": "^3.3.0",
    "eslint": "^7.21.0",
    "eslint-config-prettier": "^8.1.0",
    "eslint-plugin-deprecation": "^1.2.1",
    "eslint-plugin-eslint-comments": "^3.2.0",
    "eslint-plugin-functional": "^3.2.1",
    "eslint-plugin-import": "^2.22.1",
    "eslint-plugin-jsdoc": "^34.8.2",
    "eslint-plugin-prefer-arrow": "^1.2.3",
    "eslint-plugin-react": "^7.23.2",
    "eslint-plugin-unicorn": "^32.0.1",
    "gh-pages": "^3.1.0",
    "husky": "^6.0.0",
    "lint-staged": "^11.0.0",
    "npm-run-all": "^4.1.5",
    "npm-scripts-info": "^0.3.9",
    "nyc": "^15.1.0",
    "open-cli": "^6.0.1",
    "pinst": "^2.1.6",
    "prettier": "^2.1.1",
    "prettier-package-json": "^2.1.3",
    "standard-version": "^9.0.0",
    "trash-cli": "^4.0.0",
    "typedoc": "^0.20.35",
    "typescript": "^4.2.4"
  },
  "keywords": [
    "ansible-molecule-json",
    "megabytelabs",
    "npm",
    "professormanhattan",
    "typescript"
  ],
  "engines": {
    "node": ">=10"
  },
  "publishConfig": {
    "access": "public"
  },
  "ava": {
    "failFast": true,
    "files": [
      "!diff",
      "!test",
      "!build/module/**",
      "**/*.spec.ts"
    ],
    "ignoredByWatcher": [
      "diff",
      "test"
    ],
    "timeout": "60s",
    "typescript": {
      "rewritePaths": {
        "src/": "build/main/"
      }
    }
  },
  "funding": [
    {
      "type": "opencollective",
      "url": "https://opencollective.com/megabytelabs"
    },
    {
      "type": "patreon",
      "url": "https://www.patreon.com/ProfessorManhattan"
    }
  ],
  "lint-staged": {
    "**/*.{ts,js}": [
      "prettier --write"
    ],
    "package.json": [
      "prettier-package-json --write"
    ]
  },
  "nyc": {
    "extends": "@istanbuljs/nyc-config-typescript",
    "exclude": [
      "**/*.spec.js"
    ]
  },
  "prettier": "@megabytelabs/prettier-config",
  "scripts-info": {
    "build": "Clean and rebuild the project",
    "clean": "Remove test and build folders",
    "commit": "Automatically fix errors and guides you through the commit process",
    "cov": "Rebuild, run tests, then create and open the coverage report",
    "doc:json": "Generate API documentation in typedoc JSON format",
    "doc": "Generate HTML API documentation and open it in a browser",
    "fix": "Try to automatically fix any linting problems",
    "info": "Display information about the package scripts",
    "prepare-release": "One-step: clean, build, test, publish docs, and prep a release",
    "reset": "Delete all untracked files and reset the repo to the last commit",
    "test": "Lint and unit test the project",
    "update": "Runs .update.sh to automatically update meta files and documentation",
    "version": "Bump package.json version, update CHANGELOG.md, tag release",
    "watch": "Watch and rebuild the project on save, then rerun relevant tests",
    "watch:test": "Watch the test cases"
  },
  "standard-version": {
    "scripts": {
      "prerelease": "git add --all"
    }
  }
}

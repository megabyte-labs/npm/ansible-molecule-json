import fs from 'fs'

import stringify from 'json-stringify-pretty-compact'
import jsonfile from 'jsonfile'

import { sortResults } from './utils'

export type IData = {
  readonly compatibility: readonly (readonly string[])[]
}

export type IComparableMap = {
  readonly [key: string]: readonly string[]
}

/**
 * Write given data to a file as JSON document
 *
 * @param {string} filePath Output file path
 * @param {IData} data Data
 */
export function asJSONFile(filePath: string, data: IData): void {
  fs.writeFileSync(filePath, stringify(data), { encoding: 'utf8' })
}

/**
 * Convert to a HashMap
 *
 * @param {string[][]} data Data to convert to map
 * @returns {IComparableMap} Converted hash map
 */
const convertToMap = (data: readonly (readonly string[])[]): IComparableMap =>
  data.reduce((accumulator, row) => {
    return {
      ...accumulator,
      [`${row[0]}${row[1]}`]: row,
    }
  }, {})

/**
 * Merge data
 *
 * @param {string[][]} existingData existing data
 * @param {string[][]} newData new data
 * @returns {string[][]} merged data
 */
function merge(
  existingData: readonly (readonly string[])[],
  newData: readonly (readonly string[])[]
): readonly (readonly string[])[] {
  if (existingData.length === 0) {
    return sortResults(newData)
  }

  if (newData.length === 0) {
    return sortResults(existingData)
  }

  const existingDataMap = convertToMap(existingData)
  const newDataMap = convertToMap(newData)

  const dataToAdd = Object.keys(existingDataMap).reduce((accumulator: readonly (readonly string[])[], key) => {
    if (!newDataMap[key]) {
      return [...accumulator, existingDataMap[key]]
    }

    return accumulator
  }, [])

  const merged = [...newData, ...dataToAdd]

  const sorted = sortResults(merged)

  return sorted
}

/**
 * Inject data to the JSON file
 *
 * @param {string} filePath Target file
 * @param {string} key JSON key to inject the data
 * @param {string[][]} data Data to inject to the file
 */
export async function injectToFile(filePath: string, key: string, data: readonly (readonly string[])[]): Promise<void> {
  let temporaryData = await jsonfile.readFile(filePath)
  if (typeof temporaryData === 'string') {
    temporaryData = temporaryData.trim()
  }

  const fileData = temporaryData || {}

  if (typeof fileData === 'string') {
    throw new TypeError(`${filePath} contains string data. Injection only support for valid JSON object`)
  }
  if (Array.isArray(fileData)) {
    throw new TypeError(`${filePath} contains array data. Injection only support for valid JSON object`)
  }

  // already exist?
  if (fileData[key]) {
    // merge intelligently
    const existing = fileData[key]
    // eslint-disable-next-line functional/immutable-data
    fileData[key] = merge(existing, data)
  } else {
    const sortedData = sortResults(data)
    // eslint-disable-next-line functional/immutable-data
    fileData[key] = sortedData
  }

  fs.writeFileSync(filePath, stringify(fileData), { encoding: 'utf8' })
}

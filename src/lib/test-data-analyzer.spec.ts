import test from 'ava'
import { format } from 'date-fns'

import { TestDataAnalyzer } from './test-data-analyzer'

const getRecapData = ({
  ok = 1,
  changed = 0,
  unreachable = 0,
  failed = 0,
  skipped = 0,
  rescued = 0,
  ignored = 0,
}): readonly string[] => [
  `[32mArchlinux[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[32mCentOS-latest[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[32mCentOS-8[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[32mDebian-10[0m                  : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[32mDebian-9[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[32mUbuntu-Bionic[0m               : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[32mUbuntu-20.04[0m               : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    failed=${failed}    skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
]
const getIdempotentRecapData = ({
  ok = 1,
  changed = 0,
  unreachable = 0,
  failed = 0,
  skipped = 0,
  rescued = 0,
  ignored = 0,
}): readonly string[] => [
  `[31mArchlinux[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[31mCentOS-latest[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[31mCentOS-8[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[31mDebian-10[0m                  : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[31mDebian-9[0m                   : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[31mUbuntu-Bionic[0m               : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
  `[31mUbuntu-20.04[0m               : [32mok=${ok}   [0m changed=${changed}    unreachable=${unreachable}    [31mfailed=${failed}   [0m skipped=${skipped}    rescued=${rescued}    ignored=${ignored}`,
]

const STATUS_OK = '✅'
const STATUS_FAILED = '❌'
const STATUS_UNKNOWN = '❓'

const getResult = ({ status = STATUS_OK, idempotent = STATUS_OK }): readonly (readonly (string | number)[])[] => {
  const today = format(new Date(), 'MMMM do, yyyy')

  return [
    ['OS Family', 'OS Version', 'Status', 'Idempotent', 'Last Tested'],
    ['Archlinux', 'latest', status, idempotent, today],
    ['CentOS', '8', status, idempotent, today],
    ['CentOS', 'latest', status, idempotent, today],
    ['Debian', '9', status, idempotent, today],
    ['Debian', '10', status, idempotent, today],
    ['Ubuntu', '20.04', status, idempotent, today],
    ['Ubuntu', 'Bionic', status, idempotent, today],
  ]
}

test('Test TestDataAnalyzer', (t) => {
  const recapData = getRecapData({})

  t.deepEqual(TestDataAnalyzer.run(recapData, getIdempotentRecapData({ skipped: 1 })), getResult({}))
  t.deepEqual(TestDataAnalyzer.run(recapData, getIdempotentRecapData({ ok: 1 })), getResult({}))
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ failed: 1 })),
    getResult({ idempotent: STATUS_FAILED })
  )
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ ignored: 1 })),
    getResult({ idempotent: STATUS_FAILED })
  )
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ rescued: 1 })),
    getResult({ idempotent: STATUS_FAILED })
  )
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ rescued: 1 })),
    getResult({ idempotent: STATUS_FAILED })
  )
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ changed: 1 })),
    getResult({ idempotent: STATUS_FAILED })
  )
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ unreachable: 1 })),
    getResult({ idempotent: STATUS_FAILED })
  )
})

test('Test unknown status', (t) => {
  const recapData = getRecapData({ ok: 0 })
  t.deepEqual(
    TestDataAnalyzer.run(recapData, getIdempotentRecapData({ ok: 0 })),
    getResult({ idempotent: STATUS_UNKNOWN, status: STATUS_UNKNOWN })
  )
})

test('Test idempotent data not available', (t) => {
  const recapData = getRecapData({ ok: 1 })
  t.deepEqual(TestDataAnalyzer.run(recapData, []), getResult({ idempotent: STATUS_UNKNOWN }))
})

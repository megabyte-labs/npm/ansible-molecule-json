import fs from 'fs'
import readline from 'readline'

import { Logger } from './log'

export type ITestData = {
  readonly recapData: readonly string[]
  readonly idempotenceRecapData: readonly string[]
}

/**
 * Ansible test data loader class
 */
export class TestDataLoader {
  // eslint-disable-next-line functional/prefer-readonly-type
  private readonly recapData: string[] = []

  // eslint-disable-next-line functional/prefer-readonly-type
  private readonly idempotenceRecapData: string[] = []

  /**
   * Load test data and parse
   *
   * @param {string} filePath file path to the ansible test output
   * @returns {Promise<ITestData>} Returns a promise
   */
  // eslint-disable-next-line require-await
  public async load(filePath: string): Promise<ITestData> {
    const rl = readline.createInterface({
      input: fs.createReadStream(filePath),
      output: process.stdout,
      terminal: false,
    })

    const LEVEL_WAITING = 0
    const LEVEL_WILL_BE_STARTED = 1
    const LEVEL_STARTED = 2
    const LEVEL_RECAP = 3
    const INCREMENT = 1
    const STAGE_2 = 2

    let stage = 1
    let level = LEVEL_WILL_BE_STARTED

    return new Promise((resolve, reject) => {
      // read file line by line
      let lineNo = 1
      rl.on('line', (line) => {
        if (level === LEVEL_RECAP) {
          // is empty line?
          if (line.trim() === '') {
            Logger.debug(`Stage ${stage} ended at line: ${lineNo}`)
            // move to next stage
            stage += INCREMENT
            level = LEVEL_WAITING

            return
          }

          // collect data to buffer
          if (stage === STAGE_2) {
            // eslint-disable-next-line functional/immutable-data
            this.idempotenceRecapData.push(line)
          } else {
            // eslint-disable-next-line functional/immutable-data
            this.recapData.push(line)
          }
        } else if (stage === STAGE_2 && level === LEVEL_WAITING && /idempotence/.test(line)) {
          level = LEVEL_WILL_BE_STARTED
        } else if (level === LEVEL_STARTED && line.startsWith('PLAY RECAP')) {
          Logger.debug(`Stage ${stage} data capturing started at line: ${lineNo}`)
          // starting recap data
          level = LEVEL_RECAP
        } else if (level === LEVEL_WILL_BE_STARTED && /^PLAY \[Converge]/.test(line)) {
          Logger.debug(`Stage ${stage} started at line: ${lineNo}`)
          // starting converge data
          level = LEVEL_STARTED
        }
        lineNo += INCREMENT
      })

      rl.on('close', () => {
        resolve({
          idempotenceRecapData: this.idempotenceRecapData,
          recapData: this.recapData,
        })
      })

      rl.on('SIGTSTP', () => {
        reject(new Error('SIGTSTP received'))
      })
    })
  }
}
